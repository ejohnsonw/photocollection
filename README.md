# GENERATRON IN EASY STEPS #

This is a Demo repository for www.generatron.com
Generatron will read the model contained in spec.yaml and will use that to generate code
that is the only thing you will need to get started.

Let's do a quick demo using our http://generatronpublic.slack.com/ community. Need an invite get one at http://slack.generatron.com/

# Getting started, or better yet, Gettin' it Dunn! #

##Step 1##
Open a chat window with generatron and type

    explore https://ejohnsonw@bitbucket.org/ejohnsonw/premodemo.git PremoDemo 

And lets import another one

    explore https://ejohnsonw@bitbucket.org/ejohnsonw/photocollection.git PhotoCollection 

##Step 2##
Lets check what specs, or models you have available type

    specs

##Step 3##
Lets see what generators are available

    developers

##Step 4##
  Generate, save yourself from a whole lot of typing!

    generate using Android 

    generate using Swift 

    generate using Cobol 


##Step 5##
You should get a link where you can preview the code, and maybe you some emails waiting for you.

Some of the code have been tested and with little effort could be put in shape to run, other generators (I'm looking at you Cobol) are just proof of concepts. Use with caution, send us your feedback

##WHERE TO GO FROM HERE##
You can change models my using:

    Use [ModelName]

Create your own spec.yaml in another repo, and do "explore", remember your repo needs to be readable by public.